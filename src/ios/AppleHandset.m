#import "AppleHandset.h"

#import <Cordova/CDVAvailability.h>

@implementation AppleHandset

- (void)pluginInitialize {}

- (void)getHandsetFamily:(CDVInvokedUrlCommand *)command {

    NSString *family = @"Error";
    
    if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone) {

        switch ((int)[[UIScreen mainScreen] nativeBounds].size.height) {

            case 1136:
                //5
                NSString *family = @"5";
                break;
            case 1334:
                //6 7 8 Standard
                NSString *family = @"6";
                break;
            case 1920, 2208:
                //6+ 6s+ 7+ 8+
                NSString *family = @"6plus";
                break;
            case 2436:
                //iphone x
                NSString *family = @"iphonex";
                break;
            default:
                NSString *family = @"Unknown";
               
        }
    } else {
        NSString *family = @"Failed to get UIDevice";
    }

    CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:family];
    [self.commandDelegate sendPluginResult:result callbackId:command.callbackId]

}

@end